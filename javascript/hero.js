var hero_blurb = [
  "hatelo é uma nova rede social",
  "encontre pessoas que odeiam o que você odeia",
  "diga ao mundo o que te deixa puto",
  "a internet é feita de ódio",
  "nada aproxima mais as pessoas do que ódios mútuos"
];

var s1_blurb = [
  "Odeie o que você quiser",
  "Odeie pessoas, partidos, filmes, programas, amigos, comidas...",
  "Descubra novios ódios para compartilhar com seus inimigos"
];

var s2_blurb = [
  "Eu odeio o PT",
  "Eu odeio Game of Thrones",
  "Eu odeio a Sandra Annemberg",
  "Eu odeio o Corinthians",
  "Eu odeio comédia romântica",
  "Eu odeio uva passa",
  "Eu odeio aquela menina chata de óculos do Masterchef",
  "Eu odeio crianças",
  "Eu odeio a Veja",
  "Eu odeio sertanejo",
  "Eu odeio motoboy",
  "Eu odeio Visual Basic",
  "Eu odeio Porta dos Fundos",
  "Eu odeio calor",
  "Eu odeio sushi",
  "Eu odeio Big Brother Brasil",
  "Eu odeio a Vila Olímpia",
  "Eu odeio acordar cedo",
  "Eu odeio Jair Bolsonaro",
  "Eu odeio cheiro de verniz",
  "Eu odeio dirigir",
  "Eu odeio David Luiz",
  "Eu odeio natal",
  "Eu odeio o Paulo Velho",
  "Eu odeio mixagem de som",
  "Eu odeio o Alasca",
  "Eu odeio receber e-mail do LinkedIn",
  "Eu odeio Batman vs Superman",
  "Eu odeio água com gás",
  "Eu amo o Silvio Santos",
  "Eu odeio segunda-feira",
  "Eu odeio andar de avião",
  "Eu odeio apps de celular inúteis",
  "Eu odeio a Dilma",
  "Eu odeio baladas putz-putz",
  "Eu odeio cubos mágicos",
  "Eu odeio Legião Urbana",
  "Eu odeio camisa com gola rolê",
  "Eu odeio o Ronald Rios",
  "Eu odeio trens a vapor",
  "Eu odeio veganos",
  "Eu odeio Friends",
  "Eu odeio futebol",
  "Eu odeio o Kléber Bambam",
  "Eu odeio Snapchat",
  "Eu odeio homens",
  "Eu odeio 500 dias com ela",
  "Eu odeio a Vila Madalena",
  "Eu odeio cerveja",
  "Eu odeio Djavan",
  "Eu odeio a rede Globo",
  "Eu odeio cortar o cabelo",
  "Eu odeio identação de código usando espaço ao invés de tabs",
  "Eu odeio o ódio"
];

var s3_blurb = [
  "Lula preso amanhã",
  "Só eu que não gosto de Game of Thrones?",
  "Esse Danilo Gentili é um idiota sem graça, né?",
  "Não tem nada pior do que bolo de nozes",
  "Morte aos pontos corridos",
  "Eu odeio a internet"
];

var s2_images = [
  "lula.jpg",
  "apple.jpg",
  "david-luiz.jpg",
  "louro-jose.jpg",
  "corinthians.jpg",
  "psdb.jpg",
  "italia.jpg",
  "aquaman.jpg"
]


var heroIdx = 0;
function setHeroBlurb() {
  var b = document.getElementById("hero_blurb");
  if (heroIdx == hero_blurb.length) heroIdx = 0;
  b.innerHTML = hero_blurb[heroIdx];
  fadeIn("hero_blurb", 500);
  setTimeout( function () { fadeOut("hero_blurb", 500) }, 2500);
  heroIdx++;
}


var s1Idx = 0;
function setS1Blurb() {
  var b = document.getElementById("s1_blurb");
  if (s1Idx == s1_blurb.length) s1Idx = 0;
  b.innerHTML = s1_blurb[s1Idx];
  fadeIn("s1_blurb", 500);
  setTimeout( function () { fadeOut("s1_blurb", 300) }, 3700);
  s1Idx++;
}

var s2Idx = 0;
function setS2Blurb() {
  var b = document.getElementById("s2_blurb");
  if (s2Idx == s2_blurb.length) s2Idx = 0;
  b.innerHTML = s2_blurb[s2Idx];
  fadeIn("s2_blurb", 200);
  setTimeout( function () { fadeOut("s2_blurb", 200) }, 1300);
  s2Idx++;
}

var s2ImgIdx = 0;
function setS2Image() {
  var b = document.getElementById("folio");
  if (s2ImgIdx == s2_images.length) s2ImgIdx = 0;
  b.src = "./images/hate/" + s2_images[s2ImgIdx];
  fadeIn("folio", 500);
  setTimeout( function () { fadeOut("folio", 500) }, 4500);
  s2ImgIdx++;
}

var s3Idx = 0;
function setS3Blurb() {
  var b = document.getElementById("s3_blurb");
  if (s3Idx == s3_blurb.length) s3Idx = 0;
  b.innerHTML = s3_blurb[s3Idx];
  fadeIn("s3_blurb", 500);
  setTimeout( function () { fadeOut("s3_blurb", 300) }, 3700);
  s3Idx++;
}

var hYellow = "#FFEF85";
var hOrange = "#FF895B";

function setSectionWidth() {
  
  isPortrait = window.orientation == 0;
  var width = (isMobile && isPortrait) ? screen.width : window.innerWidth;
  //alert(width);
  var a1 = document.getElementById("trap_bottom1");
  var b1 = document.getElementById("trap_top1");
  a1.style.borderRight = b1.style.borderRight = width + "px solid " + hOrange;

  var b3 = document.getElementById("trap_top3");
  b3.style.borderRight = width + "px solid " + hYellow;
}

window.addEventListener("orientationchange", function() {
  setSectionWidth();
}, false);

var images = [];
function loadPix() {
  for (i = 1; i <= 5; i++) {
    images[i] = new Image();
    images[i].src = "./images/hero" + i + ".jpg";
  }
}

var cnt = 2;
function setImage() {
  var h = document.getElementById("hero");
  h.style.backgroundImage = "url('" + images[cnt++].src + "')";
  if (cnt > 5) cnt = 1;
  
}

function cycleImages(speed) {
  setInterval(setImage, speed);
}

function cycleBlurb(func, speed) {
    setInterval( func, speed );
}


function initialize() {
  setSectionWidth();
  loadPix();  
  cycleImages(4800); 
  setHeroBlurb();
  setS1Blurb();
  setS2Blurb();
  setS3Blurb();
  setS2Image();
  cycleBlurb( setHeroBlurb, 3500);
  cycleBlurb( setS1Blurb, 4500);
  cycleBlurb( setS2Blurb, 1500);
  cycleBlurb( setS3Blurb, 4500);
  cycleBlurb( setS2Image, 5000);
}
